import core.*;
import core.Camera;

import java.util.Scanner;

public class RoadController {
    private static double passengerCarMaxWeight = 3500.0; // kg
    private static int passengerCarMaxHeight = 2000; // mm
    private static int controllerMaxHeight = 3500; // mm

    private static int passengerCarPrice = 100; // RUB
    private static int cargoCarPrice = 250; // RUB
    private static int vehicleAdditionalPrice = 200; // RUB

    public static void main(String[] args) {
        System.out.println("Сколько автомобилей сгенерировать?");

        //Scanner - класс, позволяющий считывать данные, введенный с клавиатуры
        //System.in - поток данных, поступающий с клавиатуры (поток ввода)
        Scanner scanner = new Scanner(System.in);
        int carsCount = scanner.nextInt(); //nextInt - метод, который считывает числовое значение, введенное с клавиатуры

        for (int i = 0; i < carsCount; i++) {
            Car car = Camera.getNextCar();
            System.out.println(car);

            //Пропускаем автомобили спецтранспорта бесплатно
            if (car.isSpecial) {
                openWay();
                continue;
            }

            //Проверяем высоту и массу автомобиля, вычисляем стоимость проезда
            int price = calculatePrice(car);
            if (price == -1) {
                continue;
            }

            System.out.println("Общая сумма к оплате: " + price + " руб.");
        }
    }

    /**
     * Расчёт стоимости проезда исходя из массы и высоты
     */
    private static int calculatePrice(Car car) {
        //carHeight - присваиваем переменной значение: высота конкретной машины "car", которую передуют в аргументе функции
        int carHeight = car.height;
        double carWeight = car.weight;
        int price = 0;


        if (carHeight > controllerMaxHeight) {
            blockWay("высота вашего ТС превышает высоту пропускного пункта!");
            return -1;
        }

        if (carHeight > passengerCarMaxHeight || carWeight > passengerCarMaxWeight) {
            price = cargoCarPrice;
            if (car.hasVehicle) {
                price = price + vehicleAdditionalPrice;
            }

        } else {
            price = passengerCarPrice;
        }

        return price;

//        if (carHeight > controllerMaxHeight) {
//
//            blockWay("высота вашего ТС превышает высоту пропускного пункта!");
//            return -1;
//        //иначе если (высота машины больше макс. допустимого показателя пассаж. машины)
//        } else if (carHeight > passengerCarMaxHeight) {
//            //double weight - присваиваем переменной значение: вес конкретной машины "car", которую передуют в аргументе функции
//            double carWeight = car.weight;
//            //Грузовой автомобиль
//            //если (вес машины больше макс. допустимого показателя пассаж. машины)
//            if (carWeight > passengerCarMaxWeight) {
//               //здесь мы будем платить за машину как за грузовую
//                price = cargoCarPrice;
//                if (car.hasVehicle) {
//                    price = price + vehicleAdditionalPrice;
//                }
//            }
//            //Грузовой автомобиль
//            else {
//                price = cargoCarPrice;
//            }
//
//        } else {
//            price = passengerCarPrice;
//        }
//        return price;
    }

    /**
     * Открытие шлагбаума
     */
    private static void openWay() {
        System.out.println("Шлагбаум открывается... Счастливого пути!");
    }

    /**
     * Сообщение о невозможности проезда
     */
    private static void blockWay(String reason) {
        System.out.println("Проезд невозможен: " + reason);
    }
}